/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ftest.h"
#include "capi324v221.h"


void CBOT_main( void )
{

	LCD_open();
	LCD_clear();



	while( 1 )
	{
		print_menu();

		if ( ATTINY_get_SW_state( ATTINY_SW3 ) ){
			if(menu_pos > 0)
				menu_pos--;
		}
		else if ( ATTINY_get_SW_state( ATTINY_SW4 ) ){
			run_test(menu_pos);
		}
		else if ( ATTINY_get_SW_state( ATTINY_SW5 ) ){
			if(menu_pos < MENU_MAX)
				menu_pos++;
		}

	}

} // end CBOT_main()

void print_menu(void)
{
	LCD_printf_RC(3,0, "CEENBoT Test Utility\t" );
	if(menu_pos == 0 || menu_pos == 1 || menu_pos == 2 )
	{
		if(menu_pos == 0)
		{
			LCD_printf_RC(2,0,">");
		}
		else
		{
			LCD_printf_RC(2,0," ");
		}
		LCD_printf_RC(2,1,"LED\t");


		if(menu_pos == 1)
		{
			LCD_printf_RC(1,0,">");
		}
		else
		{
			LCD_printf_RC(1,0," ");
		}
		LCD_printf_RC(1,1,"LCD\t");


		if(menu_pos == 2)
		{
			LCD_printf_RC(0,0,">");
		}
		else
		{
			LCD_printf_RC(0,0," ");
		}
		LCD_printf_RC(0,1,"Speaker\t");
	}

	else if(menu_pos == 3 || menu_pos == 4 || menu_pos == 5)
	{
		if(menu_pos == 3)
		{
			LCD_printf_RC(2,0,">");
		}
		else
		{
			LCD_printf_RC(2,0," ");
		}
		LCD_printf_RC(2,1,"Motor\t");


		if(menu_pos == 4)
		{
			LCD_printf_RC(1,0,">");
		}
		else
		{
			LCD_printf_RC(1,0," ");
		}
		LCD_printf_RC(1,1,"Bump Sensor\t");


		if(menu_pos == 5)
		{
			LCD_printf_RC(0,0,">");
		}
		else
		{
			LCD_printf_RC(0,0," ");
		}
		LCD_printf_RC(0,1,"PSX\t");
	}

}
void run_test(int num)
{
	switch( num )
	{
	    case 0:
	        led_test();
	        break;
	    case 1:
	        lcd_test();
	        break;
	    case 2:
	        speaker_test();
	        break;
	    case 3:
	    	motor_test();
	    	break;
	    case 4:
	    	bump_test();
	    	break;
	    case 5:
	    	psx_test();
	    	break;
	    default:
	    	break;
	}
}

void led_test(void)
{
	LED_open();

	LED_set( LED_Red );
	LED_clr( LED_Green );

	for (int i = 0; i < 8; i++ )
	{
		LED_toggle( LED_Red );
		LED_toggle( LED_Green );
		TMRSRVC_delay( 500 );
	}

	LED_clr( LED_Green );
	LED_clr( LED_Red );

	LED_close();

}
 void lcd_test(void)
 {

		unsigned char i,j;
		LCD_write_cmd(0x40);
		for(i=0; i<4; i++)							// clear page 0~3
		{
			LCD_write_cmd(0xB0 + i);					// set page
			LCD_write_cmd(0x00);						// set column low nibble
			LCD_write_cmd(0x10);						// set column high nibble
			for(j=0; j<128; j++)						// clear all columns up to 128
			{
				LCD_write_data(0xFF);
				TMRSRVC_delay( 10 );
			}
		}
		for(i=0; i<4; i++)							// clear page 0~3
		{
			LCD_write_cmd(0xB0 + i);					// set page
			LCD_write_cmd(0x00);						// set column low nibble
			LCD_write_cmd(0x10);						// set column high nibble
			for(j=0; j<128; j++)						// clear all columns up to 128
			{
				LCD_write_data(0x00);
				TMRSRVC_delay( 10 );
			}
		}

	 /*LCD_clear();
	 for (char i = 33; i < 135; i++)
	 {
		 TMRSRVC_delay( 50 );
		 LCD_putchar(i);
	 }
	 TMRSRVC_delay( 3000 );*/
 }

 void speaker_test(void)
 {
	 SPKR_open( SPKR_TONE_MODE );
	 for( int i = 0; i<12; i++)
	 {
		 SPKR_play_note( i, SPKR_OCTV3, 0, 250, 80 );
	 }
	 for( int i = 11; i>=0; i--)
	 {
		 SPKR_play_note( i, SPKR_OCTV3, 0, 250, 80 );
	 }
	 SPKR_close(SPKR_TONE_MODE);
 }
 void motor_test(void)
 {
	 STEPPER_open();
	 LCD_clear();
	 LCD_printf_RC(3,0,"This test will cause");
	 LCD_printf_RC(2,0,"the motors to run");
	 LCD_printf_RC(1,0,	"Press S4 to continue");
	 while(1)
	 {
		 if ( ATTINY_get_SW_state( ATTINY_SW4 ) )
			 break;
	 }
		STEPPER_move_stnb( STEPPER_BOTH,                                    // 950 = 4 FT
				 STEPPER_FWD, 950, 300, 1000, STEPPER_BRK_OFF,   // Left  950 steps
				 STEPPER_FWD, 950, 300, 1000, STEPPER_BRK_OFF ); // Right 950 steps


				 STEPPER_wait_on( STEPPER_BOTH );

																												 // Then RIGHT left (~90-degrees)...
		 STEPPER_move_stnb( STEPPER_BOTH,
				 STEPPER_FWD, 270, 300, 1000, STEPPER_BRK_OFF,   // Left
				 STEPPER_REV, 270, 300, 1000, STEPPER_BRK_OFF ); // Right

				 //CODE

				 STEPPER_wait_on( STEPPER_BOTH );
				 // Wait 2s.
				 TMRSRVC_delay( 1000 );

		STEPPER_move_stnb( STEPPER_BOTH,                                        // 950 = 4 FT
				 STEPPER_FWD, 950, 300, 1000, STEPPER_BRK_OFF,   // Left  950 steps
				 STEPPER_FWD, 950, 300, 1000, STEPPER_BRK_OFF ); // Right 950 steps

				 //CODE

				 STEPPER_wait_on( STEPPER_BOTH );
																												 // Then RIGHT left (~90-degrees)...
		 STEPPER_move_stnb( STEPPER_BOTH,
				 STEPPER_FWD, 270, 300, 1000, STEPPER_BRK_OFF,   // Left
				 STEPPER_REV, 270, 300, 1000, STEPPER_BRK_OFF ); // Right

				 //CODE

				 STEPPER_wait_on( STEPPER_BOTH );
			 // Wait 2s.
			 TMRSRVC_delay( 1000 );



 }
void bump_test(void)
{
	LCD_clear();
	SPKR_open( SPKR_TONE_MODE );
	int np1 = 0;//note played
	int np2 = 0;

	while(!ATTINY_get_SW_state( ATTINY_SW4 ))
	{
		LCD_printf_RC(3,0,"    Left     Right   ");
		LCD_printf_RC(0,0,"      Menu (S4)      ");
		if ( ATTINY_get_IR_state( ATTINY_IR_LEFT ) )
		{
			LCD_SetCursor ( 2, 0 );
			for(int i = 0; i < 64; i++)
			{
				LCD_write_data ( 0xff );
			}

			LCD_SetCursor ( 1, 0 );
			for(int i = 0; i < 64; i++)
			{
				LCD_write_data ( 0xff );
			}
			if(np1 == 0)
			{
				SPKR_play_note( 1, SPKR_OCTV3, 0, 250, 80 );
				np1 = 1;
			}

		}
		else
		{
			LCD_SetCursor ( 2, 0 );
			for(int i = 0; i < 64; i++)
			{
				LCD_write_data ( 0x00 );
			}
			LCD_SetCursor ( 1, 0 );
			for(int i = 0; i < 64; i++)
			{
				LCD_write_data ( 0x00 );
			}


		}

		if ( ATTINY_get_IR_state( ATTINY_IR_RIGHT ) )
		{
			LCD_SetCursor ( 2, 65 );
			for(int i = 0; i < 64; i++)
			{
				LCD_write_data ( 0xff );
			}
			LCD_SetCursor ( 1, 65 );
			for(int i = 0; i < 64; i++)
			{
				LCD_write_data ( 0xff );
			}
			if(np2 == 0)
			{
				SPKR_play_note( 1, SPKR_OCTV3, 0, 250, 80 );
				np2 = 1;
			}

		}
		else
		{
			LCD_SetCursor ( 2, 65 );
			for(int i = 0; i < 64; i++)
			{
				LCD_write_data ( 0x00 );
			}
			LCD_SetCursor ( 1, 65 );
			for(int i = 0; i < 64; i++)
			{
				LCD_write_data ( 0x00 );
			}
		}
	}
	SPKR_close(SPKR_TONE_MODE);
}

void LCD_SetCursor( unsigned char ucPage, unsigned char ucColumn )
{
	LCD_write_cmd( 0xB0 + ucPage );		  		// set LCD cursor location
	LCD_write_cmd( 0x10 + ( ucColumn >> 4 ));
	LCD_write_cmd( 0x00 + ( ucColumn & 0x0f ));
}

void psx_test(void)
{
	PSXC_open();
	SPKR_open( SPKR_TONE_MODE );

	PSXC_STDATA psxc_data;
	unsigned int temp = 0;
	unsigned int temp2 = 0;


	unsigned int np1 = 0;
	unsigned int np2 = 0;
	unsigned int np3 = 0;
	unsigned int np4 = 0;
	unsigned int np5 = 0;
	unsigned int np6 = 0;
	unsigned int np7 = 0;
	unsigned int np8 = 0;

	LCD_clear();

	while(!ATTINY_get_SW_state( ATTINY_SW4 ))
	{
		if( PSXC_read( &psxc_data ) == TRUE )
		{

			LCD_printf_RC(3,0,"L1")
			if ( !( psxc_data.buttons1 & L1_BIT ) )
			{
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );
			}
			LCD_printf_RC(3,3,"R1")
			if ( !( psxc_data.buttons1 & R1_BIT ) )
			{
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );
			}
			LCD_printf_RC(3,6,"SQ")
			if ( !( psxc_data.buttons1 & SQR_BIT ) )
			{
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );
			}
			LCD_printf_RC(3,9,"CR")
			if ( !( psxc_data.buttons1 & CIR_BIT ) )
			{
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );
			}
			LCD_printf_RC(3,12,"ST")
			if ( !( psxc_data.buttons0 & STRT_BIT ) )
			{
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );
			}
			LCD_printf_RC(3,15,"DU")
			if ( !( psxc_data.buttons0 & DPAD_UP_BIT  ) )
			{
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );
			}
			LCD_printf_RC(3,18,"DL")
			if ( !( psxc_data.buttons0 & DPAD_LT_BIT  ) )
			{
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );
			}

			LCD_printf_RC(2,0,"L2")
			if ( !( psxc_data.buttons1 & L2_BIT ) )
			{
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );
			}
			LCD_printf_RC(2,3,"R2")
			if ( !( psxc_data.buttons1 & R2_BIT ) )
			{
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );
			}
			LCD_printf_RC(2,6,"TR")
			if ( !( psxc_data.buttons1 & TRI_BIT ) )
			{
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );
			}
			LCD_printf_RC(2,9," X")
			if ( !( psxc_data.buttons1 & X_BIT ) )
			{
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );
			}
			LCD_printf_RC(2,12,"SL")
			if ( !( psxc_data.buttons0 & SLCT_BIT ) )
			{
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );
			}
			LCD_printf_RC(2,15,"DD")
			if ( !( psxc_data.buttons0 & DPAD_DN_BIT  ) )
			{
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );
			}
			LCD_printf_RC(2,18,"DR")
			if ( !( psxc_data.buttons0 & DPAD_RT_BIT  ) )
			{
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );LCD_write_data ( 0xfe );
				LCD_write_data ( 0xfe );
			}

			if ( psxc_data.data_type == PSXC_ANALOG )
			{
				//left joy
				if(psxc_data.left_joy.left_right < 0)
				{
					temp = abs(psxc_data.left_joy.left_right);
					if(temp >= 120 && np1 == 0)
					{
						SPKR_play_note( 1, SPKR_OCTV3, 0, 250, 80 );
						np1 = 1;
					}

					temp = (temp * 29) / 127;

					LCD_SetCursor ( 1, 3 );
					LCD_write_data ( 0xff );

					for(int i = 28; i >= 0; i--)
					{
						if(temp > i)
							LCD_write_data ( 0x7e );
						else
							LCD_write_data ( 0x00 );

					}
					LCD_write_data( 0xff );
					for(int i = 0; i < 30; i++)
					{
						LCD_write_data ( 0x00 );
					}
					LCD_write_data( 0xff );
				}
				else
				{
					if(psxc_data.left_joy.left_right >= 120 && np2 == 0)
					{
						SPKR_play_note( 1, SPKR_OCTV3, 0, 250, 80 );
						np2 = 1;
					}
					LCD_SetCursor(1,3);
					LCD_write_data ( 0xff );

					for(int x = 0; x < 29; x++)
					{
						LCD_write_data ( 0x00 );
					}
					LCD_write_data( 0xff );

					for(int i = 0; i < 30; i++)
					{
						if( ((psxc_data.left_joy.left_right * 30) / 127) > i)
							LCD_write_data ( 0x7e );
						else
							LCD_write_data ( 0x00 );

					}
					LCD_write_data( 0xff );
				}
				//right joy
				if(psxc_data.right_joy.left_right < 0)
				{
					temp2 = abs(psxc_data.right_joy.left_right);
					if(temp2 >= 120 && np3 == 0)
					{
						SPKR_play_note( 1, SPKR_OCTV3, 0, 250, 80 );
						np3 = 1;
					}
					temp2 = (temp2 * 29) / 127;

					LCD_SetCursor ( 1, 65 );
					LCD_write_data ( 0xff );

					for(int i = 28; i >= 0; i--)
					{
						if(temp2 > i)
							LCD_write_data ( 0x7e );
						else
							LCD_write_data ( 0x00 );

					}
					LCD_write_data( 0xff );
					for(int i = 0; i < 29; i++)
					{
						LCD_write_data ( 0x00 );
					}
					LCD_write_data( 0xff );
				}
				else
				{
					if(psxc_data.right_joy.left_right >= 120 && np4 == 0)
					{
						SPKR_play_note( 1, SPKR_OCTV3, 0, 250, 80 );
						np4 = 1;
					}
					LCD_SetCursor(1,65);
					LCD_write_data( 0xff );
					for(int x = 0; x < 29; x++)
					{
						LCD_write_data ( 0x00 );
					}
					LCD_write_data( 0xff );
					for(int i = 0; i < 29; i++)
					{
						if( ((psxc_data.right_joy.left_right * 30) / 127) > i)
							LCD_write_data ( 0x7e );
						else
							LCD_write_data ( 0x00 );

					}
					LCD_write_data( 0xff );
				}

				if(psxc_data.right_joy.up_down < 0)
				{
					temp2 = abs(psxc_data.right_joy.up_down);
					if(temp2 >= 120 && np5 == 0)
					{
						SPKR_play_note( 1, SPKR_OCTV3, 0, 250, 80 );
						np5 = 1;
					}
					temp2 = (temp2 * 29) / 127;

					LCD_SetCursor ( 0, 65 );
					LCD_write_data ( 0xff );

					for(int i = 28; i >= 0; i--)
					{
						if(temp2 > i)
							LCD_write_data ( 0x7e );
						else
							LCD_write_data ( 0x00 );

					}
					LCD_write_data ( 0xff );
					for(int i = 0; i < 29; i++)
					{
						LCD_write_data ( 0x00 );
					}
					LCD_write_data ( 0xff );
				}
				else
				{
					if(psxc_data.right_joy.up_down >= 120 && np6 == 0)
					{
						SPKR_play_note( 1, SPKR_OCTV3, 0, 250, 80 );
						np6 = 1;
					}
					LCD_SetCursor(0,65);
					LCD_write_data ( 0xff );
					for(int x = 0; x < 29; x++)
					{
						LCD_write_data ( 0x00 );
					}
					LCD_write_data ( 0xff );
					for(int i = 0; i < 29; i++)
					{
						if( ((psxc_data.right_joy.up_down * 30) / 127) > i)
							LCD_write_data ( 0x7e );
						else
							LCD_write_data ( 0x00 );

					}
					LCD_write_data ( 0xff );
				}


					//left joy
					if(psxc_data.left_joy.up_down < 0)
					{
						temp = abs(psxc_data.left_joy.up_down);
						if(temp >= 120 && np7 == 0)
						{
							SPKR_play_note( 1, SPKR_OCTV3, 0, 250, 80 );
							np7 = 1;
						}
						temp = (temp * 29) / 127;

						LCD_SetCursor ( 0, 3 );
						LCD_write_data ( 0xff );
						for(int i = 28; i >= 0; i--)
						{
							if(temp > i)
								LCD_write_data ( 0x7e );
							else
								LCD_write_data ( 0x00 );

						}
						LCD_write_data ( 0xff );
						for(int i = 0; i < 30; i++)
						{
							LCD_write_data ( 0x00 );
						}
						LCD_write_data ( 0xff );
					}
					else
					{
						if(psxc_data.left_joy.up_down >= 120 && np8 == 0)
						{
							SPKR_play_note( 1, SPKR_OCTV3, 0, 250, 80 );
							np8 = 1;
						}
						LCD_SetCursor(0,3);
						LCD_write_data ( 0xff );
						for(int x = 0; x < 29; x++)
						{
							LCD_write_data ( 0x00 );
						}
						LCD_write_data ( 0xff );
						for(int i = 0; i < 30; i++)
						{
							if( ((psxc_data.left_joy.up_down * 30) / 127) > i)
								LCD_write_data ( 0x7e );
							else
								LCD_write_data ( 0x00 );

						}
						LCD_write_data ( 0xff );
					}
				//LCD_SetCursor ( 1, 0 );

				//for(int i = 0; i < 64; i++)
				//{
				//	LCD_write_data ( 0xff );
				//}
				//L_up_down = psxc_data.left_joy.left_right;
				//L_up_down = psxc_data.left_joy.up_down;
				//R_up_down = psxc_data.right_joy.up_down;
			}
		}
		else
		{
			//LCD_clear();
			//LCD_printf_RC(3,0,"PSX controller");
			//LCD_printf_RC(2,0,"not found");
		}
	}
	SPKR_close(SPKR_TONE_MODE);

}
